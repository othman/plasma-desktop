# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_shell_org\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-25 01:34+0000\n"
"PO-Revision-Date: 2023-01-09 09:58+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-IgnoreConsistency: Switch\n"
"X-POFile-SpellExtra: Ups\n"

#: contents/activitymanager/ActivityItem.qml:205
msgid "Currently being used"
msgstr "Em uso de momento"

#: contents/activitymanager/ActivityItem.qml:245
msgid ""
"Move to\n"
"this activity"
msgstr ""
"Mover para esta\n"
"actividade"

#: contents/activitymanager/ActivityItem.qml:275
msgid ""
"Show also\n"
"in this activity"
msgstr ""
"Mostrar também\n"
"nesta actividade"

#: contents/activitymanager/ActivityItem.qml:337
msgid "Configure"
msgstr "Configurar"

#: contents/activitymanager/ActivityItem.qml:356
msgid "Stop activity"
msgstr "Parar a actividade"

#: contents/activitymanager/ActivityList.qml:142
msgid "Stopped activities:"
msgstr "Actividades paradas:"

#: contents/activitymanager/ActivityManager.qml:122
msgid "Create activity…"
msgstr "Criar uma actividade…"

#: contents/activitymanager/Heading.qml:59
msgid "Activities"
msgstr "Actividades"

#: contents/activitymanager/StoppedActivityItem.qml:137
msgid "Configure activity"
msgstr "Configurar a actividade"

#: contents/activitymanager/StoppedActivityItem.qml:154
msgid "Delete"
msgstr "Apagar"

#: contents/applet/AppletError.qml:128
msgid "Sorry! There was an error loading %1."
msgstr "Infelizmente, ocorreu um erro ao carregar o %1."

#: contents/applet/AppletError.qml:166
msgid "Copy to Clipboard"
msgstr "Copiar para a Área de Transferência"

#: contents/applet/AppletError.qml:189
msgid "View Error Details…"
msgstr "Ver os Detalhes do Erro…"

#: contents/applet/CompactApplet.qml:76
msgid "Open %1"
msgstr "Abrir o %1"

#: contents/configuration/AboutPlugin.qml:21
#: contents/configuration/AppletConfiguration.qml:304
msgid "About"
msgstr "Acerca"

#: contents/configuration/AboutPlugin.qml:49
msgid "Send an email to %1"
msgstr "Enviar um e-mail para %1"

#: contents/configuration/AboutPlugin.qml:63
msgctxt "@info:tooltip %1 url"
msgid "Open website %1"
msgstr "Abrir a página Web %1"

#: contents/configuration/AboutPlugin.qml:134
msgid "Copyright"
msgstr "Copyright"

#: contents/configuration/AboutPlugin.qml:152 contents/explorer/Tooltip.qml:92
msgid "License:"
msgstr "Licença:"

#: contents/configuration/AboutPlugin.qml:155
msgctxt "@info:whatsthis"
msgid "View license text"
msgstr "Ver o texto da licença"

#: contents/configuration/AboutPlugin.qml:169
msgid "Authors"
msgstr "Autores"

#: contents/configuration/AboutPlugin.qml:180
msgid "Credits"
msgstr "Créditos"

#: contents/configuration/AboutPlugin.qml:192
msgid "Translators"
msgstr "Tradutores"

#: contents/configuration/AboutPlugin.qml:209
msgid "Report a Bug…"
msgstr "Comunicar um Erro…"

#: contents/configuration/AppletConfiguration.qml:76
msgid "Keyboard Shortcuts"
msgstr "Atalhos do Teclado"

#: contents/configuration/AppletConfiguration.qml:352
msgid "Apply Settings"
msgstr "Aplicar a Configuração"

#: contents/configuration/AppletConfiguration.qml:353
msgid ""
"The settings of the current module have changed. Do you want to apply the "
"changes or discard them?"
msgstr ""
"A configuração do módulo actual foi alterada. Deseja aplicar as alterações "
"ou esquecê-las?"

#: contents/configuration/AppletConfiguration.qml:384
msgid "OK"
msgstr "OK"

#: contents/configuration/AppletConfiguration.qml:392
msgid "Apply"
msgstr "Aplicar"

#: contents/configuration/AppletConfiguration.qml:398
msgid "Cancel"
msgstr "Cancelar"

#: contents/configuration/ConfigCategoryDelegate.qml:27
msgid "Open configuration page"
msgstr "Abrir a página de configuração"

#: contents/configuration/ConfigurationContainmentActions.qml:21
msgid "Left-Button"
msgstr "Botão Esquerdo"

#: contents/configuration/ConfigurationContainmentActions.qml:22
msgid "Right-Button"
msgstr "Botão Direito"

#: contents/configuration/ConfigurationContainmentActions.qml:23
msgid "Middle-Button"
msgstr "Botão do Meio"

#: contents/configuration/ConfigurationContainmentActions.qml:24
msgid "Back-Button"
msgstr "Botão de Recuo"

#: contents/configuration/ConfigurationContainmentActions.qml:25
msgid "Forward-Button"
msgstr "Botão de Avanço"

#: contents/configuration/ConfigurationContainmentActions.qml:27
msgid "Vertical-Scroll"
msgstr "Deslocamento Vertical"

#: contents/configuration/ConfigurationContainmentActions.qml:28
msgid "Horizontal-Scroll"
msgstr "Deslocamento Horizontal"

#: contents/configuration/ConfigurationContainmentActions.qml:30
msgid "Shift"
msgstr "Shift"

#: contents/configuration/ConfigurationContainmentActions.qml:31
msgid "Ctrl"
msgstr "Ctrl"

#: contents/configuration/ConfigurationContainmentActions.qml:32
msgid "Alt"
msgstr "Alt"

#: contents/configuration/ConfigurationContainmentActions.qml:33
msgid "Meta"
msgstr "Meta"

#: contents/configuration/ConfigurationContainmentActions.qml:95
msgctxt "Concatenation sign for shortcuts, e.g. Ctrl+Shift"
msgid "+"
msgstr "+"

#: contents/configuration/ConfigurationContainmentActions.qml:167
msgctxt "@title"
msgid "About"
msgstr "Acerca"

#: contents/configuration/ConfigurationContainmentActions.qml:182
#: contents/configuration/MouseEventInputButton.qml:13
msgid "Add Action"
msgstr "Adicionar uma Acção"

#: contents/configuration/ConfigurationContainmentAppearance.qml:69
msgid "Layout changes have been restricted by the system administrator"
msgstr ""
"As mudanças de disposição estão restritas pelo administrador de sistemas"

#: contents/configuration/ConfigurationContainmentAppearance.qml:84
msgid "Layout:"
msgstr "Disposição:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:98
msgid "Wallpaper type:"
msgstr "Tipo de papel de parede:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:119
msgid "Get New Plugins…"
msgstr "Obter Novos 'Plugins'…"

#: contents/configuration/ConfigurationContainmentAppearance.qml:189
msgid "Layout changes must be applied before other changes can be made"
msgstr ""
"As mudanças de disposição devem ser aplicadas antes de se poderem fazer "
"outras alterações"

#: contents/configuration/ConfigurationContainmentAppearance.qml:193
msgid "Apply Now"
msgstr "Aplicar Agora"

#: contents/configuration/ConfigurationShortcuts.qml:18
msgid "Shortcuts"
msgstr "Atalhos"

#: contents/configuration/ConfigurationShortcuts.qml:30
msgid "This shortcut will activate the applet as though it had been clicked."
msgstr "Este atalho irá activar o elemento como se tivesse carregado nele."

#: contents/configuration/ContainmentConfiguration.qml:27
msgid "Wallpaper"
msgstr "Papel de Parede"

#: contents/configuration/ContainmentConfiguration.qml:32
msgid "Mouse Actions"
msgstr "Acções do Rato"

#: contents/configuration/MouseEventInputButton.qml:20
msgid "Input Here"
msgstr "Escreva Aqui"

#: contents/configuration/PanelConfiguration.qml:91
#, fuzzy
#| msgid "Apply Settings"
msgid "Panel Settings"
msgstr "Aplicar a Configuração"

#: contents/configuration/PanelConfiguration.qml:97
msgid "Add Spacer"
msgstr "Adicionar um Espaço"

#: contents/configuration/PanelConfiguration.qml:100
msgid "Add spacer widget to the panel"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:107
msgid "Add Widgets…"
msgstr "Adicionar Elementos…"

#: contents/configuration/PanelConfiguration.qml:110
msgid "Open the widget selector to drag and drop widgets to the panel"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:139
msgid "Position"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:143
#: contents/configuration/PanelConfiguration.qml:254
msgid "Top"
msgstr "Topo"

#: contents/configuration/PanelConfiguration.qml:144
#: contents/configuration/PanelConfiguration.qml:256
msgid "Right"
msgstr "Direita"

#: contents/configuration/PanelConfiguration.qml:145
#: contents/configuration/PanelConfiguration.qml:254
msgid "Left"
msgstr "Esquerda"

#: contents/configuration/PanelConfiguration.qml:146
#: contents/configuration/PanelConfiguration.qml:256
msgid "Bottom"
msgstr "Fundo"

#: contents/configuration/PanelConfiguration.qml:163
msgid "Set Position..."
msgstr ""

#: contents/configuration/PanelConfiguration.qml:190
#, fuzzy
#| msgid "Panel Alignment"
msgid "Alignment"
msgstr "Alinhamento do Painel"

#: contents/configuration/PanelConfiguration.qml:255
msgid "Center"
msgstr "Centro"

#: contents/configuration/PanelConfiguration.qml:278
msgid "Length"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:296
#, fuzzy
#| msgid "Panel height:"
msgid "Fill height"
msgstr "Altura do painel:"

#: contents/configuration/PanelConfiguration.qml:296
#, fuzzy
#| msgid "Panel width:"
msgid "Fill width"
msgstr "Largura do painel:"

#: contents/configuration/PanelConfiguration.qml:297
msgid "Fit content"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:298
msgid "Custom"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:328
#, fuzzy
#| msgid "Visibility"
msgid "Visibility"
msgstr "Visibilidade"

#: contents/configuration/PanelConfiguration.qml:340
#, fuzzy
#| msgid "Always Visible"
msgid "Always visible"
msgstr "Sempre Visível"

#: contents/configuration/PanelConfiguration.qml:341
#, fuzzy
#| msgid "Auto Hide"
msgid "Auto hide"
msgstr "Auto-Esconder"

#: contents/configuration/PanelConfiguration.qml:342
msgid "Dodge windows"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:343
#, fuzzy
#| msgid "Windows Go Below"
msgid "Windows Go Below"
msgstr "As Janelas Vão Para Baixo"

#: contents/configuration/PanelConfiguration.qml:386
#, fuzzy
#| msgid "Opacity"
msgid "Opacity"
msgstr "Opacidade"

#: contents/configuration/PanelConfiguration.qml:399
msgid "Adaptive"
msgstr "Adaptativo"

#: contents/configuration/PanelConfiguration.qml:400
#, fuzzy
#| msgid "Opaque"
msgid "Opaque"
msgstr "Opaco"

#: contents/configuration/PanelConfiguration.qml:401
#, fuzzy
#| msgid "Translucent"
msgid "Translucent"
msgstr "Translúcido"

#: contents/configuration/PanelConfiguration.qml:425
msgid "Style"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:436
#, fuzzy
#| msgid "Floating Panel"
msgid "Floating"
msgstr "Painel Flutuante"

#: contents/configuration/PanelConfiguration.qml:510
#, fuzzy
#| msgid "Panel width:"
msgid "Panel Width:"
msgstr "Largura do painel:"

#: contents/configuration/PanelConfiguration.qml:510
#, fuzzy
#| msgid "Right"
msgid "Height:"
msgstr "Direita"

#: contents/configuration/PanelConfiguration.qml:544
#, fuzzy
#| msgid "Shortcut"
msgid "Focus shortcut:"
msgstr "Atalho"

#: contents/configuration/PanelConfiguration.qml:554
msgid "Press this keyboard shortcut to move focus to the Panel"
msgstr ""
"Carregue nesta combinação de teclas para colocar o Painel em primeiro plano"

#: contents/configuration/PanelConfiguration.qml:570
#, fuzzy
#| msgid "Remove Panel"
msgctxt "@action:button Delete the panel"
msgid "Delete Panel"
msgstr "Remover o Painel"

#: contents/configuration/PanelConfiguration.qml:573
msgid "Remove this panel; this action is undo-able"
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:30
msgid "Drag to change maximum height."
msgstr "Arraste para mudar a altura máxima."

#: contents/configuration/panelconfiguration/Ruler.qml:30
msgid "Drag to change maximum width."
msgstr "Arraste para mudar a largura máxima."

#: contents/configuration/panelconfiguration/Ruler.qml:30
#: contents/configuration/panelconfiguration/Ruler.qml:31
msgid "Double click to reset."
msgstr "Faça duplo-click para reiniciar."

#: contents/configuration/panelconfiguration/Ruler.qml:31
msgid "Drag to change minimum height."
msgstr "Arraste para mudar a altura mínima."

#: contents/configuration/panelconfiguration/Ruler.qml:31
msgid "Drag to change minimum width."
msgstr "Arraste para mudar a largura mínima."

#: contents/configuration/panelconfiguration/Ruler.qml:78
msgid ""
"Drag to change position on this screen edge.\n"
"Double click to reset."
msgstr ""
"Arraste para mudar a posição neste extremo do ecrã.\n"
"Faça duplo-click para reiniciar."

#: contents/configuration/ShellContainmentConfiguration.qml:19
msgid "Panels and Desktops Management"
msgstr "Gestão dos Painéis e Ecrãs"

#: contents/configuration/ShellContainmentConfiguration.qml:34
msgid ""
"You can drag Panels and Desktops around to move them to different screens."
msgstr ""
"Poderá arrastar os painéis e os ecrã à vontade para os mover para ecrãs "
"diferentes."

#: contents/configuration/ShellContainmentConfiguration.qml:43
msgid "Close"
msgstr "Fechar"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:179
msgid "Swap with Desktop on Screen %1"
msgstr "Trocar com o Ecrã no Ecrã Físico %1"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:180
msgid "Move to Screen %1"
msgstr "Mover para o Ecrã %1"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:194
#, fuzzy
#| msgid "Remove Panel"
msgid "Remove Desktop"
msgstr "Remover o Painel"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:195
msgid "Remove Panel"
msgstr "Remover o Painel"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:268
msgid "%1 (primary)"
msgstr "%1 (primário)"

#: contents/explorer/AppletAlternatives.qml:65
msgid "Alternative Widgets"
msgstr "Elementos Alternativos"

#: contents/explorer/AppletDelegate.qml:42
msgid "Unsupported Widget"
msgstr ""

#: contents/explorer/AppletDelegate.qml:188
msgid "Undo uninstall"
msgstr "Desfazer a desinstalação"

#: contents/explorer/AppletDelegate.qml:189
msgid "Uninstall widget"
msgstr "Desinstalar o elemento"

#: contents/explorer/Tooltip.qml:101
msgid "Author:"
msgstr "Autor:"

#: contents/explorer/Tooltip.qml:109
msgid "Email:"
msgstr "E-mail:"

#: contents/explorer/Tooltip.qml:128
msgid "Uninstall"
msgstr "Desinstalar"

#: contents/explorer/WidgetExplorer.qml:118
#: contents/explorer/WidgetExplorer.qml:192
msgid "All Widgets"
msgstr "Todos os Elementos"

#: contents/explorer/WidgetExplorer.qml:144
msgid "Widgets"
msgstr "Elementos"

#: contents/explorer/WidgetExplorer.qml:152
msgid "Get New Widgets…"
msgstr "Obter Elementos Novos…"

#: contents/explorer/WidgetExplorer.qml:203
msgid "Categories"
msgstr "Categorias"

#: contents/explorer/WidgetExplorer.qml:285
msgid "No widgets matched the search terms"
msgstr "Não existem elementos correspondentes aos termos da pesquisa"

#: contents/explorer/WidgetExplorer.qml:285
msgid "No widgets available"
msgstr "Sem elementos disponíveis"

#, fuzzy
#~| msgid "Maximize Panel"
#~ msgctxt "@action:button Make the panel as big as it can be"
#~ msgid "Maximize"
#~ msgstr "Maximizar o Painel"

#, fuzzy
#~| msgid "Delete"
#~ msgctxt "@action:button Delete the panel"
#~ msgid "Delete"
#~ msgstr "Apagar"

#, fuzzy
#~| msgid "Floating Panel"
#~ msgid "Floating:"
#~ msgstr "Painel Flutuante"

#~ msgid "More Options…"
#~ msgstr "Mais Opções…"

#~ msgctxt "Minimize the length of this string as much as possible"
#~ msgid "Drag to move"
#~ msgstr "Arraste para mover"

#~ msgctxt "@info:tooltip"
#~ msgid "Use arrow keys to move the panel"
#~ msgstr "Usar as teclas de cursor para mover o painel"

#~ msgid "Switch"
#~ msgstr "Mudança"

#, fuzzy
#~| msgid "Windows Go Below"
#~ msgid "Windows Above"
#~ msgstr "As Janelas Vão Para Baixo"

#, fuzzy
#~| msgid "Windows Can Cover"
#~ msgid "Windows In Front"
#~ msgstr "As Janelas Podem Cobrir"
