# Translation of plasma_shell_org.kde.plasma__desktop.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2013, 2014, 2015, 2016, 2017.
msgid ""
msgstr ""
"Project-Id-Version: plasma_shell_org.kde.plasma__desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-25 01:34+0000\n"
"PO-Revision-Date: 2017-09-25 19:53+0200\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#: contents/activitymanager/ActivityItem.qml:205
msgid "Currently being used"
msgstr "Тренутно се користи"

#: contents/activitymanager/ActivityItem.qml:245
#, fuzzy
#| msgid "Stop activity"
msgid ""
"Move to\n"
"this activity"
msgstr "Заустави активност"

#: contents/activitymanager/ActivityItem.qml:275
msgid ""
"Show also\n"
"in this activity"
msgstr ""

#: contents/activitymanager/ActivityItem.qml:337
msgid "Configure"
msgstr "Подеси"

#: contents/activitymanager/ActivityItem.qml:356
msgid "Stop activity"
msgstr "Заустави активност"

#: contents/activitymanager/ActivityList.qml:142
msgid "Stopped activities:"
msgstr "Заустављене активности:"

#: contents/activitymanager/ActivityManager.qml:122
#, fuzzy
#| msgid "Create activity..."
msgid "Create activity…"
msgstr "Направи активност..."

#: contents/activitymanager/Heading.qml:59
msgid "Activities"
msgstr "Активности"

#: contents/activitymanager/StoppedActivityItem.qml:137
msgid "Configure activity"
msgstr "Подеси активност"

#: contents/activitymanager/StoppedActivityItem.qml:154
msgid "Delete"
msgstr "Обриши"

#: contents/applet/AppletError.qml:128
msgid "Sorry! There was an error loading %1."
msgstr ""

#: contents/applet/AppletError.qml:166
msgid "Copy to Clipboard"
msgstr ""

#: contents/applet/AppletError.qml:189
msgid "View Error Details…"
msgstr ""

#: contents/applet/CompactApplet.qml:76
msgid "Open %1"
msgstr ""

#: contents/configuration/AboutPlugin.qml:21
#: contents/configuration/AppletConfiguration.qml:304
msgid "About"
msgstr ""

#: contents/configuration/AboutPlugin.qml:49
msgid "Send an email to %1"
msgstr ""

#: contents/configuration/AboutPlugin.qml:63
msgctxt "@info:tooltip %1 url"
msgid "Open website %1"
msgstr ""

#: contents/configuration/AboutPlugin.qml:134
msgid "Copyright"
msgstr ""

#: contents/configuration/AboutPlugin.qml:152 contents/explorer/Tooltip.qml:92
msgid "License:"
msgstr "Лиценца:"

#: contents/configuration/AboutPlugin.qml:155
msgctxt "@info:whatsthis"
msgid "View license text"
msgstr ""

#: contents/configuration/AboutPlugin.qml:169
#, fuzzy
#| msgid "Author:"
msgid "Authors"
msgstr "Аутор:"

#: contents/configuration/AboutPlugin.qml:180
msgid "Credits"
msgstr ""

#: contents/configuration/AboutPlugin.qml:192
msgid "Translators"
msgstr ""

#: contents/configuration/AboutPlugin.qml:209
msgid "Report a Bug…"
msgstr ""

#: contents/configuration/AppletConfiguration.qml:76
#, fuzzy
#| msgid "Keyboard shortcuts"
msgid "Keyboard Shortcuts"
msgstr "Пречице са тастатуре"

#: contents/configuration/AppletConfiguration.qml:352
msgid "Apply Settings"
msgstr "Примени поставке"

#: contents/configuration/AppletConfiguration.qml:353
msgid ""
"The settings of the current module have changed. Do you want to apply the "
"changes or discard them?"
msgstr ""
"Измењене су поставке текућег модула. Желите ли да их примените или одбаците?"

#: contents/configuration/AppletConfiguration.qml:384
msgid "OK"
msgstr "У реду"

#: contents/configuration/AppletConfiguration.qml:392
msgid "Apply"
msgstr "Примени"

#: contents/configuration/AppletConfiguration.qml:398
msgid "Cancel"
msgstr "Одустани"

#: contents/configuration/ConfigCategoryDelegate.qml:27
msgid "Open configuration page"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:21
msgid "Left-Button"
msgstr "Лево дугме"

#: contents/configuration/ConfigurationContainmentActions.qml:22
msgid "Right-Button"
msgstr "Десно дугме"

#: contents/configuration/ConfigurationContainmentActions.qml:23
msgid "Middle-Button"
msgstr "Средње дугме"

#: contents/configuration/ConfigurationContainmentActions.qml:24
msgid "Back-Button"
msgstr "Дугме назад"

#: contents/configuration/ConfigurationContainmentActions.qml:25
msgid "Forward-Button"
msgstr "Дугме напред"

#: contents/configuration/ConfigurationContainmentActions.qml:27
msgid "Vertical-Scroll"
msgstr "Усправно клизање"

#: contents/configuration/ConfigurationContainmentActions.qml:28
msgid "Horizontal-Scroll"
msgstr "Водоравно клизање"

#: contents/configuration/ConfigurationContainmentActions.qml:30
msgid "Shift"
msgstr "Shift"

#: contents/configuration/ConfigurationContainmentActions.qml:31
msgid "Ctrl"
msgstr "Ctrl"

#: contents/configuration/ConfigurationContainmentActions.qml:32
msgid "Alt"
msgstr "Alt"

#: contents/configuration/ConfigurationContainmentActions.qml:33
msgid "Meta"
msgstr "Meta"

#: contents/configuration/ConfigurationContainmentActions.qml:95
msgctxt "Concatenation sign for shortcuts, e.g. Ctrl+Shift"
msgid "+"
msgstr "+"

#: contents/configuration/ConfigurationContainmentActions.qml:167
msgctxt "@title"
msgid "About"
msgstr ""

#: contents/configuration/ConfigurationContainmentActions.qml:182
#: contents/configuration/MouseEventInputButton.qml:13
msgid "Add Action"
msgstr "Додај радњу"

#: contents/configuration/ConfigurationContainmentAppearance.qml:69
msgid "Layout changes have been restricted by the system administrator"
msgstr ""

#: contents/configuration/ConfigurationContainmentAppearance.qml:84
msgid "Layout:"
msgstr "Распоред:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:98
#, fuzzy
#| msgid "Wallpaper Type:"
msgid "Wallpaper type:"
msgstr "Тип тапета:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:119
msgid "Get New Plugins…"
msgstr ""

#: contents/configuration/ConfigurationContainmentAppearance.qml:189
msgid "Layout changes must be applied before other changes can be made"
msgstr "Промене распореда морају да се примене пре даљих измена."

#: contents/configuration/ConfigurationContainmentAppearance.qml:193
#, fuzzy
#| msgid "Apply now"
msgid "Apply Now"
msgstr "Примени сада"

#: contents/configuration/ConfigurationShortcuts.qml:18
#, fuzzy
#| msgid "Keyboard shortcuts"
msgid "Shortcuts"
msgstr "Пречице са тастатуре"

#: contents/configuration/ConfigurationShortcuts.qml:30
msgid "This shortcut will activate the applet as though it had been clicked."
msgstr ""

#: contents/configuration/ContainmentConfiguration.qml:27
msgid "Wallpaper"
msgstr "Тапет"

#: contents/configuration/ContainmentConfiguration.qml:32
msgid "Mouse Actions"
msgstr "Радње мишем"

#: contents/configuration/MouseEventInputButton.qml:20
msgid "Input Here"
msgstr "Унос овде"

#: contents/configuration/PanelConfiguration.qml:91
#, fuzzy
#| msgid "Apply Settings"
msgid "Panel Settings"
msgstr "Примени поставке"

#: contents/configuration/PanelConfiguration.qml:97
msgid "Add Spacer"
msgstr "Додај размакницу"

#: contents/configuration/PanelConfiguration.qml:100
msgid "Add spacer widget to the panel"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:107
#, fuzzy
#| msgid "Add Widgets..."
msgid "Add Widgets…"
msgstr "Додај виџете..."

#: contents/configuration/PanelConfiguration.qml:110
msgid "Open the widget selector to drag and drop widgets to the panel"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:139
msgid "Position"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:143
#: contents/configuration/PanelConfiguration.qml:254
msgid "Top"
msgstr "Врх"

#: contents/configuration/PanelConfiguration.qml:144
#: contents/configuration/PanelConfiguration.qml:256
msgid "Right"
msgstr "Десно"

#: contents/configuration/PanelConfiguration.qml:145
#: contents/configuration/PanelConfiguration.qml:254
msgid "Left"
msgstr "Лево"

#: contents/configuration/PanelConfiguration.qml:146
#: contents/configuration/PanelConfiguration.qml:256
msgid "Bottom"
msgstr "Дно"

#: contents/configuration/PanelConfiguration.qml:163
msgid "Set Position..."
msgstr ""

#: contents/configuration/PanelConfiguration.qml:190
#, fuzzy
#| msgid "Panel Alignment"
msgid "Alignment"
msgstr "Поравнање панела"

#: contents/configuration/PanelConfiguration.qml:255
msgid "Center"
msgstr "Средина"

#: contents/configuration/PanelConfiguration.qml:278
msgid "Length"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:296
#, fuzzy
#| msgid "Panel Alignment"
msgid "Fill height"
msgstr "Поравнање панела"

#: contents/configuration/PanelConfiguration.qml:296
#, fuzzy
#| msgid "Uninstall widget"
msgid "Fill width"
msgstr "Деинсталирај виџет"

#: contents/configuration/PanelConfiguration.qml:297
msgid "Fit content"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:298
msgid "Custom"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:328
#, fuzzy
#| msgid "Visibility"
msgid "Visibility"
msgstr "Видљивост"

#: contents/configuration/PanelConfiguration.qml:340
#, fuzzy
#| msgid "Always Visible"
msgid "Always visible"
msgstr "Увек видљив"

#: contents/configuration/PanelConfiguration.qml:341
#, fuzzy
#| msgid "Auto Hide"
msgid "Auto hide"
msgstr "Аутоматско сакривање"

#: contents/configuration/PanelConfiguration.qml:342
msgid "Dodge windows"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:343
#, fuzzy
#| msgid "Windows Go Below"
msgid "Windows Go Below"
msgstr "Прозори иду испод"

#: contents/configuration/PanelConfiguration.qml:386
msgid "Opacity"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:399
msgid "Adaptive"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:400
msgid "Opaque"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:401
#, fuzzy
#| msgid "Always Visible"
msgid "Translucent"
msgstr "Увек видљив"

#: contents/configuration/PanelConfiguration.qml:425
msgid "Style"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:436
msgid "Floating"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:510
#, fuzzy
#| msgid "Panel Alignment"
msgid "Panel Width:"
msgstr "Поравнање панела"

#: contents/configuration/PanelConfiguration.qml:510
#, fuzzy
#| msgid "Right"
msgid "Height:"
msgstr "Десно"

#: contents/configuration/PanelConfiguration.qml:544
#, fuzzy
#| msgid "Keyboard shortcuts"
msgid "Focus shortcut:"
msgstr "Пречице са тастатуре"

#: contents/configuration/PanelConfiguration.qml:554
msgid "Press this keyboard shortcut to move focus to the Panel"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:570
#, fuzzy
#| msgid "Remove Panel"
msgctxt "@action:button Delete the panel"
msgid "Delete Panel"
msgstr "Уклони панел"

#: contents/configuration/PanelConfiguration.qml:573
msgid "Remove this panel; this action is undo-able"
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:30
msgid "Drag to change maximum height."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:30
msgid "Drag to change maximum width."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:30
#: contents/configuration/panelconfiguration/Ruler.qml:31
msgid "Double click to reset."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:31
msgid "Drag to change minimum height."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:31
msgid "Drag to change minimum width."
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:78
msgid ""
"Drag to change position on this screen edge.\n"
"Double click to reset."
msgstr ""

#: contents/configuration/ShellContainmentConfiguration.qml:19
msgid "Panels and Desktops Management"
msgstr ""

#: contents/configuration/ShellContainmentConfiguration.qml:34
msgid ""
"You can drag Panels and Desktops around to move them to different screens."
msgstr ""

#: contents/configuration/ShellContainmentConfiguration.qml:43
msgid "Close"
msgstr "Затвори"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:179
msgid "Swap with Desktop on Screen %1"
msgstr ""

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:180
msgid "Move to Screen %1"
msgstr ""

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:194
#, fuzzy
#| msgid "Remove Panel"
msgid "Remove Desktop"
msgstr "Уклони панел"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:195
msgid "Remove Panel"
msgstr "Уклони панел"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:268
msgid "%1 (primary)"
msgstr ""

#: contents/explorer/AppletAlternatives.qml:65
msgid "Alternative Widgets"
msgstr "Алтернативни виџети"

#: contents/explorer/AppletDelegate.qml:42
msgid "Unsupported Widget"
msgstr ""

#: contents/explorer/AppletDelegate.qml:188
msgid "Undo uninstall"
msgstr "Опозови деинсталирање"

#: contents/explorer/AppletDelegate.qml:189
msgid "Uninstall widget"
msgstr "Деинсталирај виџет"

#: contents/explorer/Tooltip.qml:101
msgid "Author:"
msgstr "Аутор:"

#: contents/explorer/Tooltip.qml:109
msgid "Email:"
msgstr "Е‑пошта:"

#: contents/explorer/Tooltip.qml:128
msgid "Uninstall"
msgstr "Деинсталирај"

#: contents/explorer/WidgetExplorer.qml:118
#: contents/explorer/WidgetExplorer.qml:192
#, fuzzy
#| msgid "Widgets"
msgid "All Widgets"
msgstr "Виџети"

#: contents/explorer/WidgetExplorer.qml:144
msgid "Widgets"
msgstr "Виџети"

#: contents/explorer/WidgetExplorer.qml:152
#, fuzzy
#| msgid "Get new widgets"
msgid "Get New Widgets…"
msgstr "Добави нове виџете"

#: contents/explorer/WidgetExplorer.qml:203
msgid "Categories"
msgstr "Категорије"

#: contents/explorer/WidgetExplorer.qml:285
msgid "No widgets matched the search terms"
msgstr ""

#: contents/explorer/WidgetExplorer.qml:285
msgid "No widgets available"
msgstr ""

#, fuzzy
#~| msgid "Maximize Panel"
#~ msgctxt "@action:button Make the panel as big as it can be"
#~ msgid "Maximize"
#~ msgstr "Максимизуј панел"

#, fuzzy
#~| msgid "Delete"
#~ msgctxt "@action:button Delete the panel"
#~ msgid "Delete"
#~ msgstr "Обриши"

#, fuzzy
#~| msgid "More Settings..."
#~ msgid "More Options…"
#~ msgstr "Више поставки..."

#~ msgid "Switch"
#~ msgstr "Пребаци"

#, fuzzy
#~| msgid "Windows Go Below"
#~ msgid "Windows Above"
#~ msgstr "Прозори иду испод"

#, fuzzy
#~| msgid "Windows Can Cover"
#~ msgid "Windows In Front"
#~ msgstr "Прозори могу да прекрију"

#~ msgid "Search..."
#~ msgstr "Тражи..."

#~ msgid "Screen Edge"
#~ msgstr "Ивица екрана"
